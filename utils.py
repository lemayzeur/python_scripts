from copy import deepcopy
from celery import shared_task
from io import BytesIO as StringIO
from django.core.files import File
from PIL import Image
from django.template.defaultfilters import slugify
from django.core.files.uploadedfile import InMemoryUploadedFile
from code9haiti.conf import DEFAULT_PAGE
from django.utils.translation import ugettext_lazy as _
from django.utils.formats import date_format
from django.conf import settings
import datetime
import re
import django
import hashlib
import random
import string
import os
import pytz
import qrcode
import boto3
# import logging

# SCRIPT TO COMPRESS PHOTO
class CompressImage:
	VERSION = 1.0
	AUTHOR = "Lub Lorry Lamysere"
	HELP = '''
		Compress Image needs an object, ex.. CompressImage(obj), obj needs to be an Image or Memory Image File
		with the object, you can call the following methods: resize(size,box), crop(size,box), rotate(degree).
		render method is there for generate the Memory File. you can also render the Memory File inside those method : (resize, crop, rotate), by just put 'render=True' inside.
		In case you want to generate a physical Image File, just the save methode, it takes the path as parameter.
		Thank you
	'''

	def __init__(self,img,ext="JPEG",quality=95,filename=None):
		if img is None:
			raise ValueError("Image can't be None, provide an Image or an InMemoryUploadedFile")
		if not isinstance(quality,int) or quality > 100:
			raise ValueError("Quality must be an integer less than 100")
		try:
			self.img = Image.open(StringIO(img.read())) # InMemoryUploadedFile
			self.filename = os.path.splitext(img.name)[0]
		except Exception as e:
			self.img = img # PIL Image File
			if not filename:
				self.filename = "$"#os.path.splitext(img.name)[0]
		self.file = img
		if filename:
			self.filename = filename
		try:
			if self.img.mode != 'RGB':
				self.img = self.img.convert('RGB')
		except:
			pass
		self.ext = ext
		self.quality = quality
		try:
			self.img_ratio = self.img.size[0] / float(self.img.size[1])
		except:
			self.img_ratio = 1
	def __str__(self):
		return "%s.jpg" % self.filename # InMemoryUploadedFile has name

	def get_file(self):
		return self.file

	def render(self,filename=None):
		img = self.img
		output = StringIO()
		img.save(output, format=self.ext, quality=self.quality)
		output.seek(0)

		if not filename:
			img = self.file = InMemoryUploadedFile(output,'ImageField',\
				"%s.jpg" % self.filename , 'image/jpeg', output.__sizeof__(), None)
		else:
			img = self.file = InMemoryUploadedFile(output,'ImageField',\
				"%s.jpg" % filename , 'image/jpeg', output.__sizeof__(), None)
		return img

	def resize_and_crop(self,size=(),box=None,crop_type='middle',fill="blank",render=False):
		img = self.resize(size(),box,crop_type,fill)
		self.img = img.crop(size(),box,crop_type,fill)
		if render:
			return self.render()
		return self.__class__(img,filename=self.filename)

	def crop(self,size=(),box=None,crop_type='middle',fill="blank",render=False):			
		ratio = size[0] / float(size[1])
		img_ratio = self.img_ratio
		img = self.img
		if fill == 'blank':
			if ratio > img_ratio:
				# Crop in the top, middle or bottom
				if crop_type == 'top':
					if not box:
						box = (0, 0, img.size[0], size[1])
				elif crop_type == 'middle':
					if not box:
						box = (0, int(round((img.size[1] - size[1]) / 2)), img.size[0],
						int(round((img.size[1] + size[1]) / 2)))
				elif crop_type == 'bottom':
					if not box:
						box = (0, img.size[1] - size[1], img.size[0], img.size[1])
				else :
					raise ValueError('ERROR: invalid value for crop_type')
				
				self.img = img.crop(box)

			elif ratio < img_ratio:
				if crop_type == 'top':
					if not box:
						box = (0, 0, size[0], img.size[1])
				elif crop_type == 'middle':
					if not box:
						box = (int(round((img.size[0] - size[0]) / 2)), 0,
						int(round((img.size[0] + size[0]) / 2)), img.size[1])
				elif crop_type == 'bottom':
					if not box:
						box = (img.size[0] - size[0], 0, img.size[0], img.size[1])
				else :
					raise ValueError('ERROR: invalid value for crop_type')

			self.img = img.crop(box)

		elif fill == 'black':
			fill_color = (0,0,0,0)
			self.img = Image.new('RGB', (size[0], size[1]), fill_color)
			box = (int(round((size[0] - img.size[0]) / 2)), int(round((size[1] - img.size[1]) / 2)))
			self.img.paste(img, box)
		else:
			raise ValueError('ERROR: fill can not be None')

		if render:
			return self.render()
		return self.__class__(self.img,filename=self.filename)

	def rotate(self,degree,render=False):
		degree = float(degree)
		if not degree.is_integer():
			raise ValueError("Value Error, Degree must be integer , between -360 & 360")
		elif degree < -360 or degree > 360:
			raise ValueError("Value Error, Degree must be between -360 & 360")

		self.img = self.img.rotate(degree*-1,expand=1)
	
		if render:
			return self.render()
		return self.__class__(self.img,filename=self.filename)

	def resize(self,size=(),box=None,render=False):
		ratio = size[0] / float(size[1])
		img_ratio = self.img_ratio
		img = self.img
		
		if ratio > img_ratio:
			if not box:
				box = (size[0], int(round(size[0] * img.size[1] / img.size[0])))
			self.img = img.resize(box,Image.ANTIALIAS)
		elif ratio < img_ratio:
			if not box:
				box = (int(round(size[1] * img.size[0] / img.size[1])), size[1])
			self.img = img.resize(box,Image.ANTIALIAS)
		else:
			if not box:
				box = (size[0], size[1])
			self.img = img.resize(box,Image.ANTIALIAS)

		if render:
			return self.render()
		return self.__class__(self.img,filename=self.filename)

	def dimensions(self):
		return self.img.size

	def get_ratio(self):
		return self.ratio

	def save(self):
		# for DJANGO, if not self.img = img
		img = Image.open(StringIO(self.img.read()))
		img.save("%s-new.jpg" % os.path.splitext(self.filename)[0],\
			format=self.ext,quality=self.quality)

def create_code(instance,nb,new_hash=None,field="code"):
	_hash = hashlib.sha1(str(random.random()).encode()).hexdigest()[:nb]

	if new_hash is not None:
		_hash = new_hash
	else:
		_hash = hashlib.sha1(str(random.random()).encode()).hexdigest()[:nb]

	Klass = instance.__class__
	lookups = {field:_hash}
	qs_exists = Klass.objects.filter(**lookups).exists()
	if qs_exists:
		_hash = hashlib.sha1(str(random.random()).encode()).hexdigest()[:nb]
		return create_code(instance,_hash)
	return _hash

def create_slug(instance,new_slug=None,field_name="name"):
	def generator(size=10,chars=string.ascii_letters + string.digits):
		return ''.join(random.choice(chars) for _ in range(size))

	if new_slug is not None:
		slug = new_slug[:70]
	else:
		slug = slugify(getattr(instance,field_name))[:70] # params : Title or Name or Subject
		if not slug:
			slug = generator(10)

	Klass = instance.__class__
	qs_exists = Klass.objects.filter(slug=slug).exists()
	if qs_exists:
		new_slug = "%s-%s" % (slug,generator(4))
		if len(new_slug) > 60:
			new_slug = new_slug[:60]
		return create_slug(instance,new_slug,field_name)
	return slug

def file_exists(file):
	try:
		if not file.name:
			return False
		return file.storage.exists(file.name)
	except:
		return False

def money_format(number):
	return '{:,.2f}'.format(float(number))

def clean_float(float_number):
	'''float with 0 as trailing, remove the 0: 12.0==12'''
	if not isinstance(float_number,float) and not isinstance(float_number,int):
		raise ValueError("Number is not float")
	float_number = '{0:g}'.format(float(float_number))
	if not float_number.isdigit():
		return money_format(float_number)
	return float_number


def handle_duration(value,unit):
	from code9haiti.conf import DAY,HOUR,MINUTE,WEEK,MONTH,SECOND
	if not value or not unit:
		raise ValueError
	if not isinstance(value,int) and value.isdigit():
		value = int(value)
	if unit == DAY:
		lookups = {'days':value}
	elif unit == HOUR:
		lookups = {'hours':value}
	elif unit == MINUTE:
		print(value)
		lookups = {'minutes':value}
	elif unit == SECOND:
		lookups = {'seconds':value}
	elif unit == WEEK:
		lookups = {'weeks':value}
	elif unit == MONTH:
		value *= 30
		lookups = {'days':value}
	else:
		raise ValueError
	return datetime.timedelta(**lookups)

# QUERY JOIN in SQL
def append_to_dict_keys(word,d):
	dct = d.copy()
	for key,val in list(dct.items()):
		dct[word + key]= dct.pop(key)
	return dct

# QUERY JOIN in SQL
def prepend_to_dict_keys(word,d):
	dct = d.copy()
	for key,val in list(dct.items()):
		dct[key + word]= dct.pop(key)
	return dct

def duration_to_timedelta(duration,unit):
	# error_logger = logging.getLogger('error_logger')
	MIN = MAX = None
	if  '-' in duration:
		MIN,MAX = duration.strip().split("-")
		MIN = int(float(MIN))
		MAX = int(float(MAX))
	else:
		try:
			MIN = MAX = int(float(duration))
		except ValueError as e:
			MIN = MAX = 0
	if unit == 'H':
		return datetime.timedelta(hours=MIN),datetime.timedelta(hours=MAX)
	if unit == 'M':
		return datetime.timedelta(minutes=MIN),datetime.timedelta(minutes=MAX)
	return datetime.timedelta(days=MIN),datetime.timedelta(days=MAX)

def timedelta_to_duration(d1,d2):
	# error_logger = logging.getLogger('error_logger')
	MIN = MAX = None
	if  '-' in duration:
		MIN,MAX = duration.strip().split("-")
		MIN = int(MIN)
		MAX = int(MAX)
	else:
		try:
			MIN = MAX = int(duration)
		except ValueError:
			MIN = MAX = 0
	if unit == 'H':
		return datetime.timedelta(hours=MIN),datetime.timedelta(hours=MAX)
	if unit == 'M':
		return datetime.timedelta(minutes=MIN),datetime.timedelta(minutes=MAX)
	return datetime.timedelta(seconds=MIN),datetime.timedelta(seconds=MAX)

def removespace(obj):
	if isinstance(obj,str):
		return re.sub(r'\s+', '', obj)
	return obj

# Generate QRCODE
def render_qrcode(id=None,code=None):
	qr = qrcode.QRCode(
		version=1,
		error_correction=qrcode.constants.ERROR_CORRECT_L,
		box_size=10,
		border=1,
	)
	qr.add_data(code)
	qr.make(fit=True)

	img = qr.make_image()

	_buffer = StringIO()
	img.save(_buffer)
	filename = 'qrocde-%s.jpg' % (id)
	filebuffer = InMemoryUploadedFile(
		_buffer, None, filename, 'image/jpg',None, None)

	return filebuffer 

# Convert PDF to JPG
def pdf_to_jpg(file,file_name,from_bytes=True):
	from pdf2image import convert_from_path,convert_from_bytes
	if settings.DEBUG:
		pages = convert_from_path(file.path, 500,fmt='jpeg' )
	else:
		client = boto3.client('s3',aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
					aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
	
		absolute_path = settings.AWS_PUBLIC_MEDIA_LOCATION + "/" + file.name
		s3_response_object = client.get_object(Bucket=settings.AWS_STORAGE_BUCKET_NAME, Key=absolute_path)
		object_content = s3_response_object['Body'].read()
		pages = convert_from_bytes(object_content, 500,fmt='jpeg' )

	if pages:
		page = pages[0]
		width, height = page.size
		page = page.crop([300, 345, width-200, height-250])
		_buffer = StringIO()
		page.save(_buffer, format='JPEG')
		file_name = file_name + '.jpg'
		filebuffer = InMemoryUploadedFile(
			_buffer, None, file_name, 'image/jpg',None, None)
		return filebuffer 

def duration_str(duration):
	if isinstance(duration,datetime.timedelta):
		total_seconds = int(duration.total_seconds())
		days = total_seconds // 86400
		hours = (total_seconds % 86400) // 3600
		minutes = (total_seconds % 3600) // 60
		seconds = total_seconds % 60

		if days and days > 0:
			if hours and hours > 0:
				if minutes and minutes > 0:
					return _('%(days)s days %(hours)s hours %(minutes)s min') % {'days':days, 'hours':hours, 'minutes':minutes}
				return _('%(days)s days %(hours)s hours') % {'days':days, 'hours':hours}
			elif minutes and minutes > 0:
				return _('%(days)s days %(minutes)s min') % {'days':days, 'minutes':minutes}
			return _('%(days)s days') % {'days':days}
		elif hours and hours > 0:
			if minutes and minutes > 0:
				return _('%(hours)s hours %(minutes)s min') % {'hours':hours, 'minutes':minutes}
			return _('%(hours)s hours') % {'hours':hours}
		elif minutes and minutes > 0:
			if seconds and seconds > 0:
				return _('%(minutes)s min %(seconds)s sec') % {'minutes':minutes,'seconds':seconds}
			return _('%(minutes)s min') % {'minutes':minutes}
		return _('%(seconds)s sec') % {'seconds':seconds}